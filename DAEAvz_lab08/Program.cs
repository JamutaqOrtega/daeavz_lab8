﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAEAvz_lab08
{
    class Program
    {
        public static DataClasses1DataContext context = new DataClasses1DataContext();

        static void Main(string[] args)
        {
            Console.WriteLine("INTO TO LINQ:\n---------");
            IntroToLINQ();
            Console.WriteLine("\nDATASOURCE:\n---------");
            DataSource();
            Console.WriteLine("\nFILTERING:\n---------");
            Filtering();
            Console.WriteLine("\nORDERING:\n---------");
            Ordering();
            Console.WriteLine("\nGROUPING:\n------------");
            Grouping();
            Console.WriteLine("\nGROUPING 2:\n------------");
            Grouping2();
            Console.WriteLine("\nJOINING:\n---------");
            Joining();

            Console.Read();
        }

        //METODOS
        static void IntroToLINQ()
        {
            //1. DATA SOURCE
            int[] numbers = new int[7] { 1, 2, 3, 4, 5, 6, 7 };

            //2. QUERY CREATION
            var _numberQuery =
                from num in numbers
                where (num % 2 == 0)
                select num;

            //lambda version
            var numberQuery = numbers.Where(n => (n % 2 == 0)).ToList();

            //3. QUERY EXECUTION
            foreach (int num in _numberQuery)
            {
                Console.WriteLine("{0,1}", num);
            }
        }

        static void DataSource()
        {
            var _queryAllCustomers = from cust in context.clientes
                                     select cust;

            //lambda
            var queryAllCustomers = context.clientes.Select(cust => cust).ToList();

            foreach (var item in queryAllCustomers)
            {
                Console.WriteLine(item.NombreCompañia);
            }
        }

        static void Filtering()
        {
            var _queryLondonCustomers = from cust in context.clientes
                                        where cust.Ciudad == "Londres"
                                        select cust;

            //lambda
            var queryLondonCustomers = context.clientes.Where(cust => cust.Ciudad == "Londres").ToList();

            foreach (var item in queryLondonCustomers)
            {
                Console.WriteLine(item.Ciudad);
            }
        }

        static void Ordering()
        {
            var _queryLondonCustomers3 = from cust in context.clientes
                                         where cust.Ciudad == "Londres"
                                         orderby cust.NombreCompañia ascending
                                         select cust;

            //lambda
            var queryLondonCustomers3 = context.clientes.Where(cust => cust.Ciudad == "Londres").OrderBy(x => x.NombreCompañia).ToList();

            foreach (var item in queryLondonCustomers3)
            {
                Console.WriteLine(item.NombreCompañia);
            }
        }

        static void Grouping()
        {
            var _queryCustomerByCity = from cust in context.clientes
                                       group cust by cust.Ciudad;

            //lambda
            var queryCustomerByCity = context.clientes.GroupBy(c => c.Ciudad).ToList();

            foreach (var customerGroup in queryCustomerByCity)
            {
                Console.WriteLine(customerGroup.Key);
                foreach (cliente customer in customerGroup)
                {
                    Console.WriteLine("\t{0}", customer.NombreCompañia);
                }
            }
        }

        static void Grouping2()
        {
            var _custQuery = from cust in context.clientes
                             group cust by cust.Ciudad into custGroup
                             where custGroup.Count() > 2
                             orderby custGroup.Key
                             select custGroup;

            //lambda
            var custQuery = context.clientes.GroupBy(c => c.Ciudad).Where(x => x.Count() > 2).OrderBy(y => y.Key).ToList();

            foreach (var item in custQuery)
            {
                Console.WriteLine(item.Key);
            }
        }

        static void Joining()
        {
            var _innerJoinQuery = from cust in context.clientes
                                  join dist in context.Pedidos on cust.idCliente equals dist.IdCliente
                                  select new { CustomerName = cust.NombreCompañia, DistributorName = dist.PaisDestinatario };

            //lambda
            var innerJoinQuery = context.clientes
                .Join(context.Pedidos,
                    cust => cust.idCliente,
                    dist => dist.IdCliente,
                    (cust, dist) => new { CustomerName = cust.NombreCompañia, DistributorName = dist.PaisDestinatario })
                .ToList();

            foreach (var item in innerJoinQuery)
            {
                Console.WriteLine(item.CustomerName);
            }
        }
    }
}
